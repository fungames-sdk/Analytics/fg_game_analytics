# GameAnalytics

## Integration Steps

1) **"Install"** or **"Upload"** FG GameAnalytics plugin from the FunGames Integration Manager or download it from <a href="https://drive.google.com/uc?export=download&id=1-r2PwNTvAGKd8hTO96A1_mCdxQNwrldt" target="_blank">here</a>.

2) Follow the instructions in the **"Install External Plugin"** section to import GA SDK.

3) Click on the **"Prefabs and Settings"** button in the FunGames Integration Manager to fill up your scene with required components and create the Settings asset.

4) To finish your integration, follow the **Account and Settings** section.

## Install External Plugin

After importing the FG GameAnalytics module from the Integration Manager window, you will find the last compatible version of GA SDK in the _Assets > FunGames_Externals > Analytics_ folder. Double click on the .unitypackage file to install it.

If you wish to install a newest version of their SDK, you can also download it  <a href="https://gameanalytics.com/docs/s/article/Integration-Unity-SDK" target="_blank">here</a>.

**Note that the versions of included external SDKs are the latest tested and approved by our development team. We recommend using these versions to avoid any type of discrepencies. If, for any reason, you already have a different version of the plugin installed in your project that you wish to keep, please advise our dev team.**

## Account and Settings

First you will have to create a <a href="https://go.gameanalytics.com/home" target="_blank">GameAnalytics</a> account: 

- Go to your account / Create one if you don’t have one
- Go to your studio account / Create one if you don’t have one
- Go to your Game page / Create one if you don’t have one
- Go to Settings > Users > Invite User (see screenshot below)
- Add the Email : **analytics@tap-nation.io**

Then we will receive emails from GameAnalytics to have the data from the chosen game.

Once done, select the **FGGameAnalyticsSettings** asset in the Resources folder (_Assets > Resources > FunGames > FGGameAnalyticsSettings_) and fill it with the credentials of your app from the GA Dashboard. 

To test the integration please first run your game by having it built and running on your phone as the data sending only works on mobile. Once the game is launched you can see the events on your GameAnalytics panel on the tab RealTime -> Live feed